# README #

This Repo contains 13 files, all found in the Downloads section:
 - Word Doc Documentation for the IoT Enterprise Demo Pump (read this first)
 - A python script, cumulocitytoWEMO.py, that runs on a raspberry pi to connect a wemo switch to the cumulocity dashboard
 - A python script, PumptoCumulocity.py, that runs on a raspberry pi to connect via RS-485 from the pump through to the cumulocity dashboard
 - A Codesys v3.5 project archive, software that runs on the IFM As-i Gateway device
 - 3 XML config files that define how the lineRecorder agent runs on the Microsoft Azure VM
 - 2 Manuals relating to using Modbus RS 485 comms. with the pump
 - A word doc file containing important screenshots from these manuals
 - A python script, modbus.py, that runs on a raspberry pi to allow for the querying of modbus registers
 - An PDF conntaining modbus register addresses on the pump
 - A word doc MODBUS CONTROL listing where I am at with controlling the pump remotely.